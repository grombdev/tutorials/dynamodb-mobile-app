import json
from scripts import dynamodb_cfg

def bulk_load_table():
    dynamodb = dynamodb_cfg.GetDynamoDBResource()
    table = dynamodb.Table('quick-photos')

    items = []

    with open('/code/scripts/items.json', 'r') as f:
        for row in f:
            items.append(json.loads(row))

    with table.batch_writer() as batch:
        for item in items:
            batch.put_item(Item=item)
