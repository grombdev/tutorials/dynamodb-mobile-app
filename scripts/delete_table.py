from scripts import dynamodb_cfg

def delete_table():
    dynamodb = dynamodb_cfg.GetDynamoDBClient()

    try:
        dynamodb.delete_table(TableName='quick-photos')
        print("Table deleted successfully.")
    except Exception as e:
        print("Could not delete table. Please try again in a moment. Error:")
        print(e)
