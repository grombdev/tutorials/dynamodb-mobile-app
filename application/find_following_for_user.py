from application.entities import Friendship
from scripts import dynamodb_cfg

def __find_following_for_user(username):
    dynamodb = dynamodb_cfg.GetDynamoDBClient()
    resp = dynamodb.query(
        TableName='quick-photos',
        IndexName='InvertedIndex',
        KeyConditionExpression="SK = :sk",
        ExpressionAttributeValues={
            ":sk": { "S": "#FRIEND#{}".format(username) }
        },
        ScanIndexForward=True
    )

    return [Friendship(item) for item in resp['Items']]

def find_following_for_user():
    USERNAME = "haroldwatkins"
    follows = __find_following_for_user(USERNAME)

    print("Users followed by {}:".format(USERNAME))
    for follow in follows:
        print(follow)
