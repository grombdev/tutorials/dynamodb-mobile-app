import datetime
from scripts import dynamodb_cfg

def __follow_user(followed_user, following_user):
    dynamodb = dynamodb_cfg.GetDynamoDBClient()
    user = "USER#{}".format(followed_user)
    friend = "#FRIEND#{}".format(following_user)
    user_metadata = "#METADATA#{}".format(followed_user)
    friend_user = "USER#{}".format(following_user)
    friend_metadata = "#METADATA#{}".format(following_user)
    try:
        resp = dynamodb.transact_write_items(
            TransactItems=[
                {
                    "Put": {
                        "TableName": "quick-photos",
                        "Item": {
                            "PK": {"S": user},
                            "SK": {"S": friend},
                            "followedUser": {"S": followed_user},
                            "followingUser": {"S": following_user},
                            "timestamp": {"S": datetime.datetime.now().isoformat()},
                        },
                        "ConditionExpression": "attribute_not_exists(SK)",
                        "ReturnValuesOnConditionCheckFailure": "ALL_OLD",
                    }
                },
                {
                    "Update": {
                        "TableName": "quick-photos",
                        "Key": {"PK": {"S": user}, "SK": {"S": user_metadata}},
                        "UpdateExpression": "SET followers = followers + :i",
                        "ExpressionAttributeValues": {":i": {"N": "1"}},
                        "ReturnValuesOnConditionCheckFailure": "ALL_OLD",
                    }
                },
                {
                    "Update": {
                        "TableName": "quick-photos",
                        "Key": {"PK": {"S": friend_user}, "SK": {"S": friend_metadata}},
                        "UpdateExpression": "SET following = following + :i",
                        "ExpressionAttributeValues": {":i": {"N": "1"}},
                        "ReturnValuesOnConditionCheckFailure": "ALL_OLD",
                    }
                },
            ]
        )
        print("User {} is now following user {}".format(following_user, followed_user))
        return True
    except Exception as e:
        print(e)
        print("Could not add follow relationship")

def follow_user():
    FOLLOWED_USER = 'tmartinez'
    FOLLOWING_USER = 'john42'
    __follow_user(FOLLOWED_USER, FOLLOWING_USER)