from application.entities import User, Photo
from scripts import dynamodb_cfg

def __fetch_user_and_photos(username):
    dynamodb = dynamodb_cfg.GetDynamoDBClient()
    resp = dynamodb.query(
        TableName='quick-photos',
        KeyConditionExpression="PK = :pk AND SK BETWEEN :metadata AND :photos",
        ExpressionAttributeValues={
            ":pk": { "S": "USER#{}".format(username) },
            ":metadata": { "S": "#METADATA#{}".format(username) },
            ":photos": { "S": "PHOTO$" },
        },
        ScanIndexForward=True
    )

    user = User(resp['Items'][0])
    user.photos = [Photo(item) for item in resp['Items'][1:]]

    return user

def fetch_user_and_photos():
    USER = "jacksonjason"
    user = __fetch_user_and_photos(USER)

    print(user)
    for photo in user.photos:
        print(photo)

