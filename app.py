from scripts import add_inverted_index, bulk_load_table, create_table, delete_table
from application import add_reaction, fetch_photo_and_reactions, fetch_user_and_photos, find_and_enrich_following_for_user, find_following_for_user, follow_user

def main():
    create_table.create_table()
    bulk_load_table.bulk_load_table()
    fetch_user_and_photos.fetch_user_and_photos()

    add_inverted_index.add_inverted_index()
    fetch_photo_and_reactions.fetch_photo_and_reactions()
    find_following_for_user.find_following_for_user()

    find_and_enrich_following_for_user.find_and_enrich_following_for_user()

    add_reaction.add_reaction()
    follow_user.follow_user()

    delete_table.delete_table()

    print("Exiting...")

if __name__ == "__main__":
    main()